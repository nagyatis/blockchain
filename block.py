import datetime
import hashlib


class Block:
    def __init__(self, data, prev_block_hash = 64*"0"):
        self.__data = data
        self.__prev_block_hash = prev_block_hash
        self.__timestamp = datetime.datetime.now().astimezone().isoformat()
        self.__nonce = None
        self.__block_hash = None
        self.mine_block_hash()

    def get_data(self):
        return self.__data

    def get_prev_block_hash(self):
        return self.__prev_block_hash

    def get_timestamp(self):
        return self.__timestamp

    def get_nonce(self):
        return self.__nonce

    def get_block_hash(self):
        return self.__block_hash

    def mine_block_hash(self):
        print("*** BÁNYÁSZAT INDUL! ***")
        counter = 0
        while True:
            hash = hashlib.sha256(f"{self.__data}@{self.__prev_block_hash}@{self.__timestamp}@{counter}".encode())
            hash_hexdigest = hash.hexdigest()
            # print(f"nonce: {counter} - block hash: {hash_hexdigest}")
            if hash_hexdigest.startswith("0000"):
                self.__nonce = counter
                self.__block_hash = hash_hexdigest
                print("*** KÉSZ! ***")
                print(f"timestamp:       {self.__timestamp}")
                print(f"nonce:           {self.__nonce}")
                print(f"prev block hash: {self.__prev_block_hash}")
                print(f"block hash:      {self.__block_hash}")
                break
            counter = counter + 1
        return None
