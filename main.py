
from blockchain import *
# from pprint import pprint

if __name__ == '__main__':
    print(100 * "=")
    print("A blokkláncok működésének alapjai (beadandó)\nNagy Attila Sándor (AIBWCZ)\nMATE Műszaki Intézet")
    print(100 * "=")

    blockchain = Blockchain()

    while True:
        print("\nMi a legyen a következő blokk tartalma? (Utasítások: kilépés=Q!; kilépés mentéssel=QS!;)")
        data = input("")
        data = data.strip()
        print("\n")

        if data == "Q!;":
            print("*** KILÉPÉS ***")
            break

        if data == "QS!;":
            print("*** KILÉPÉS ***")
            blockchain.save_to_file()
            break

        if len(data) > 0:
            blockchain.add_block(data)
            print(100 * "-")
