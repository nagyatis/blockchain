from block import *
import json
from typing import List


class Blockchain:
    def __init__(self):
        self.__blockchain = []

    def add_block(self, data):
        prev_block_hash = 64 * "0"
        if len(self.__blockchain) > 0:
            prev_block_hash = self.__blockchain[-1].get_block_hash()
        self.__blockchain.append(Block(data, prev_block_hash))

    def save_to_file(self):
        with open("blockchain.json", "w", encoding="utf-8") as f:
            json.dump([x.__dict__ for x in self.__blockchain], f)
        print("*** A MENTÉS MEGTÖRTÉNT! ***")
